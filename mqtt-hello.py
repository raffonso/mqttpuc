from mqtt import MQTTClient
from net import Network
import pycom
import utime
import _thread
from pysense import Pysense
from MPL3115A2 import MPL3115A2,ALTITUDE,PRESSURE
from SI7006A20 import SI7006A20
from LTR329ALS01 import LTR329ALS01



pycom.heartbeat(False)
pycom.rgbled(0x7f0000) # red

network = Network()
network.lte_connect()
pycom.rgbled(0x7f7f00) # yellow


def sub_cb(topic, msg):
   topic = topic.decode()
   msg = msg.decode()

   if topic == '/led':
       cor_hex = int(msg, 16)
       pycom.rgbled(cor_hex)



client = MQTTClient("device_id", "172.25.1.14", port=1883)
client.set_callback(sub_cb)
client.connect()
pycom.rgbled(0x007f00) # green
client.subscribe(topic="/led")

py = Pysense()
mp = MPL3115A2(py,mode=ALTITUDE) # Returns height in meters. Mode may also be set to PRESSURE, returning a value in Pascals
ls = LTR329ALS01(py)
si = SI7006A20(py)

def read_forever():
    while True:
        luz = ls.light()
        media = (luz[0] + luz[1]) / 2
        light = str(int(media))
        client.publish(topic="/light", msg=light)
        print("Light: " + light)
        utime.sleep(1)

_thread.start_new_thread(read_forever, tuple())

while True:
    client.check_msg()
    utime.sleep(0.01)
