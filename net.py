from network import LTE
import utime


class Network:
    def __init__(self):
        self.lte = LTE()

    def _send_at_cmd_pretty(self, cmd, debug=False):
        response = self.lte.send_at_cmd(cmd).split('\r\n')
        if debug:
            for line in response:
                print(line)

    def _lte_setup(self, band, earfcn, apn):
        self.lte.reset()
        self._send_at_cmd_pretty('AT+CFUN=0')
        self._send_at_cmd_pretty('AT!="clearscanconfig"')
        self._send_at_cmd_pretty('AT!="addscanfreq band={} dl-earfcn={}"'.format(band, earfcn))
        self._send_at_cmd_pretty('AT!="zsp0:npc 1"')
        self._send_at_cmd_pretty('AT+CGDCONT=1,"IP","{}"'.format(apn))
        self._send_at_cmd_pretty('AT+CEMODE=0')
        self._send_at_cmd_pretty('AT+CFUN=1')

    def _wait_lte_attach(self, debug=False):
        while not self.lte.isattached():
            utime.sleep(1)
            self._send_at_cmd_pretty('AT!="showphy"')
            self._send_at_cmd_pretty('AT!="fsm"')
            self._send_at_cmd_pretty('AT+CEREG?')

    def lte_connect(self, band=3, earfcn=1250, apn='lte.labcuritiba.br'):
        self._lte_setup(band, earfcn, apn)
        self._wait_lte_attach()

        self.lte.connect()
        print('Connecting...')

        while not self.lte.isconnected():
            pass

        print('Connecting...[DONE]')
